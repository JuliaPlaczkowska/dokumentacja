Instalacja
===========

Upewnij się, że masz zainstalowane Docker Engine oraz Doker Compose

1. Zdefiniuj zależności w aplikacji
2. Dodaj testowy endpoint
3. Stwórz Dockerfile
4. Dodaj plik docker-compose.yml, w kt\orym zdefiniujesz serwisy aplikacji, np. web serwis i baza danych
5. Zbuduj obrazy serwisów komendą ``docker-compose build``
6. Wystartuj swoją aplikację używając komendy ``docker-compose up``
