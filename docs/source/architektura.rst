Architektura
=============
Diagram UML
----
.. uml::



    interface CustomerRepository
    CustomerController o-- Customer
    CustomerRepository --* Lab3Application
    CustomerRepository <-- Customer
    CustomerRepository --* CustomerController

    class Customer {
      -Long id
      -String firstName
      -String lastName
      +toString()
    }

    class CustomerController {
      -CustomerRepository : customerRepository
      +Customer : getUserById()
      +Iterable<Customer> : getCustomers(Long customerId)

    }

    class Lab3Application {
    +CommandLineRunner : dataBootstrap(CustomerRepository : repository)
    }

Diagram przypadków użycia
---------
.. uml::

    left to right direction
    skinparam packageStyle rectangle
    actor User

    rectangle app {
    User --> (Search customer)
    (Search customer) .> (Get all customers) : include

    User --> (Add customer)
    (Search customer) .> (Find customer by ID) : include

    }