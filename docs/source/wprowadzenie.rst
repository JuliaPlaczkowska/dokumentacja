Wprowadzenie - Czym jest Docker oraz Docker Compose
====================================================

Docker zapewnia możliwość pakowania i uruchamiania aplikacji w tak zwanym kontenerze.
Dzięki temu aplikację stworzoną na jednym komputerz emożna uruchomić na innym urządzeniu bez
modyfikowania zależności w projekcie.

Docker Compose to narzędzie do budowania i uruchamiani wielokontenerowych aplikacji. Serwisy aplikacji
konfiguruje się w pliku YAML dzięki czemu można je zbuildować i uruchomić.