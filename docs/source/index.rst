.. lab3 documentation master file, created by
   sphinx-quickstart on Sun Jun  6 15:12:25 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to lab3's documentation!
================================

.. toctree::
   :maxdepth: 2

   wprowadzenie
   instalacja
   architektura
   api


