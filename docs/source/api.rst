API
===

Wyświetlenie jednego klienta podając jego ID
---------------------------------------------
GET ``http://localhost:8080/customers/{id}``


Wyświetlenie wszystkich klientów z bazy
----------------------------------------
GET ``http://localhost:8080/customers``